/**
 * 名称：burlesco.user.js
 * 地址：https://burles.co/userscript/burlesco.user.js
 *
 ******** 以下为 tamperJS 自动生成的 rewrite 相关信息，可能需要根据情况适当调整 ********

[rewrite]
.*:\/\/www\.bloomberg\.com\/.* url script-response-body burlesco.user.js
.*:\/\/correio\.rac\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.nsctotal\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/www\.economist\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.estadao\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/foreignpolicy\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.fivewall\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.folha\.uol\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.folha\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/gauchazh\.clicrbs\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.zh\.clicrbs\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/api\.clicrbs\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.gazetadopovo\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/ogjs\.infoglobo\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.jota\.info\/.* url script-response-body burlesco.user.js
.*:\/\/www\.jornalnh\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/www\.netdeal\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.nytimes\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.nyt\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.oglobo\.globo\.com\/.* url script-response-body burlesco.user.js
.*:\/\/www\.rbsonline\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/api\.tinypass\.com\/.* url script-response-body burlesco.user.js
.*:\/\/cdn\.tinypass\.com\/.* url script-response-body burlesco.user.js
.*:\/\/dashboard\.tinypass\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.washingtonpost\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.exame\.abril\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/www\.eltiempo\.com\/.* url script-response-body burlesco.user.js
.*:\/\/super\.abril\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/veja\.abril\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/quatrorodas\.abril\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.uol\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/www\.uol\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.wsj\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.ft\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.gramophone\.co\.uk\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.folhadelondrina\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.wired\.com\/.* url script-response-body burlesco.user.js
.*:\/\/www\.jornalvs\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.br18\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.diariopopular\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.haaretz\.com\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.haaretz\.co\.il\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.diarinho\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.diariodaregiao\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.correio24horas\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.dgabc\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/crusoe\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.em\.com\.br\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.forbes\.pl\/.* url script-response-body burlesco.user.js
.*:\/\/.*\.newsweek\.pl\/.* url script-response-body burlesco.user.js

[mitm]
, www.bloomberg.com, correio.rac.com.br, *.nsctotal.com.br, www.economist.com, *.estadao.com.br, foreignpolicy.com, *.fivewall.com.br, *.folha.uol.com.br, *.folha.com.br, gauchazh.clicrbs.com.br, *.zh.clicrbs.com.br, api.clicrbs.com.br, *.gazetadopovo.com.br, ogjs.infoglobo.com.br, *.jota.info, www.jornalnh.com.br, www.netdeal.com.br, *.nytimes.com, *.nyt.com, *.oglobo.globo.com, www.rbsonline.com.br, api.tinypass.com, cdn.tinypass.com, dashboard.tinypass.com, *.washingtonpost.com, *.exame.abril.com.br, www.eltiempo.com, super.abril.com.br, veja.abril.com.br, quatrorodas.abril.com.br, *.uol.com.br, www.uol, *.wsj.com, *.ft.com, *.gramophone.co.uk, *.folhadelondrina.com.br, *.wired.com, www.jornalvs.com.br, *.br18.com.br, *.diariopopular.com.br, *.haaretz.com, *.haaretz.co.il, *.diarinho.com.br, *.diariodaregiao.com.br, *.correio24horas.com.br, *.dgabc.com.br, crusoe.com.br, *.em.com.br, *.forbes.pl, *.newsweek.pl

 ********
 * 工具: tamperJS BY @elecV2
 * 频道: https://t.me/elecV2
 *
**/

let body = $response.body

if (/<\/html>|<\/body>/.test(body)) {
  body = body.replace('</body>', `
<script>
function GM_openInTab(e){return window.open(e)}function GM_addStyle(e){"use strict";let t=document.getElementsByTagName("head")[0];if(t){let s=document.createElement("style");return s.setAttribute("type","text/css"),s.textContent=e,t.appendChild(s),s}return null}const GM_log=console.log;function GM_xmlhttpRequest(e){"use strict";let t=new XMLHttpRequest;if(__setupRequestEvent(e,t,"abort"),__setupRequestEvent(e,t,"error"),__setupRequestEvent(e,t,"load"),__setupRequestEvent(e,t,"progress"),__setupRequestEvent(e,t,"readystatechange"),t.open(e.method,e.url,!e.synchronous,e.user||"",e.password||""),e.overrideMimeType&&t.overrideMimeType(e.overrideMimeType),e.headers)for(let s in e.headers)Object.prototype.hasOwnProperty.call(e.headers,s)&&t.setRequestHeader(s,e.headers[s]);let s=e.data?e.data:null;return e.binary?t.sendAsBinary(s):t.send(s)}function __setupRequestEvent(e,t,s){"use strict";e["on"+s]&&t.addEventListener(s,function(n){let r={responseText:t.responseText,responseXML:t.responseXML,readyState:t.readyState,responseHeaders:null,status:null,statusText:null,finalUrl:null};switch(s){case"progress":r.lengthComputable=n.lengthComputable,r.loaded=n.loaded,r.total=n.total;break;case"error":break;default:if(4!=t.readyState)break;r.responseHeaders=t.getAllResponseHeaders(),r.status=t.status,r.statusText=t.statusText}e["on"+s](r)})}const GM_info={script:{name:"elecV2",namespace:"https://t.me/elecV2"},uuid:"47c23801-2b5c-4dbc-88a0-636297fd6b86"},__GM_STORAGE_PREFIX=["",GM_info.script.namespace,GM_info.script.name,""].join("***");function GM_deleteValue(e){"use strict";localStorage.removeItem(__GM_STORAGE_PREFIX+e)}function GM_getValue(e,t){"use strict";let s=localStorage.getItem(__GM_STORAGE_PREFIX+e);return null===s&&void 0!==t?t:s}function GM_listValues(){"use strict";let e=__GM_STORAGE_PREFIX.length,t=[];for(let s=0;s<localStorage.length;s++){let n=localStorage.key(s);n.substr(0,e)===__GM_STORAGE_PREFIX&&t.push(n.substr(e))}return t}function GM_setValue(e,t){"use strict";localStorage.setItem(__GM_STORAGE_PREFIX+e,t)}function GM_getResourceURL(e){"use strict";return"greasemonkey-script:"+GM_info.uuid+"/"+e}
</script>
<script>const elecJSPack = function(elecV2){

// ==UserScript==
// @name         Burlesco
// @namespace    https://burles.co/
// @version      11.18
// @description  Leia notícias sem ser assinante, burle o paywall
// @author       rodorgas & AugustoResende
// @supportURL   https://burles.co
// @icon64       https://burles.co/userscript/icon.png
// Atenção:      Caso algum site não funcione logo após a instalação, limpe o cache do navegador.
// @grant        GM_webRequest
// @grant        GM_xmlhttpRequest
// @connect      gauchazh.clicrbs.com.br
// @connect      static.infoglobo.com.br
// @connect      cdn.tinypass.com
// @match        *://www.bloomberg.com/*
// @match        *://correio.rac.com.br/*
// @match        *://*.nsctotal.com.br/*
// @match        *://www.economist.com/*
// @match        *://*.estadao.com.br/*
// @match        *://foreignpolicy.com/*
// @match        *://*.fivewall.com.br/*
// @match        *://*.folha.uol.com.br/*
// @match        *://*.folha.com.br/*
// @match        *://gauchazh.clicrbs.com.br/*
// @match        *://*.zh.clicrbs.com.br/*
// @match        *://api.clicrbs.com.br/*
// @match        *://*.gazetadopovo.com.br/*
// @match        *://ogjs.infoglobo.com.br/*
// @match        *://*.jota.info/*
// @match        *://www.jornalnh.com.br/*
// @match        *://www.netdeal.com.br/*
// @match        *://*.nytimes.com/*
// @match        *://*.nyt.com/*
// @match        *://*.oglobo.globo.com/*
// @match        *://www.rbsonline.com.br/*
// @match        *://api.tinypass.com/*
// @match        *://cdn.tinypass.com/*
// @match        *://dashboard.tinypass.com/*
// @match        *://*.washingtonpost.com/*
// @match        *://*.exame.abril.com.br/*
// @match        *://www.eltiempo.com/*
// @match        *://super.abril.com.br/*
// @match        *://veja.abril.com.br/*
// @match        *://quatrorodas.abril.com.br/*
// @match        *://*.uol.com.br/*
// @match        *://www.uol/*
// @match        *://*.wsj.com/*
// @match        *://*.ft.com/*
// @match        *://*.gramophone.co.uk/*
// @match        *://*.folhadelondrina.com.br/*
// @match        *://*.wired.com/*
// @match        *://www.jornalvs.com.br/*
// @match        *://*.br18.com.br/*
// @match        *://*.diariopopular.com.br/*
// @match        *://*.haaretz.com/*
// @match        *://*.haaretz.co.il/*
// @match        *://*.diarinho.com.br/*
// @match        *://*.diariodaregiao.com.br/*
// @match        *://*.correio24horas.com.br/*
// @match        *://*.dgabc.com.br/*
// @match        *://crusoe.com.br/*
// @match        *://*.em.com.br/*
// @match        *://*.forbes.pl/*
// @match        *://*.newsweek.pl/*
// @webRequest [{"selector":"*://correio-static.cworks.cloud/vendor/bower_components/paywall.js/paywall.js*","action":"cancel"},{"selector":{"include":"*://paywall.folha.uol.com.br/*","exclude":"*://paywall.folha.uol.com.br/status.php"} ,"action":"cancel"},{"selector":"*://static.folha.uol.com.br/paywall/*","action":"cancel"},{"selector":"*://ogjs.infoglobo.com.br/*/js/controla-acesso-aux.js","action":"cancel"},{"selector":"*://static.infoglobo.com.br/paywall/register-piano/*/scripts/nova-tela-register.js","action":"cancel"},{"selector":"*://www.netdeal.com.br/*","action":"cancel"},{"selector":"*://correio.rac.com.br/includes/js/novo_cp/fivewall.js*","action":"cancel"},{"selector":"*://dashboard.tinypass.com/xbuilder/experience/load*","action":"cancel"},{"selector":"*://*.fivewall.com.br/*","action":"cancel"},{"selector":"*://www.rbsonline.com.br/cdn/scripts/SLoader.js","action":"cancel"},{"selector":"*://*.nytimes.com/js/mtr.js","action":"cancel"},{"selector":"*://*.washingtonpost.com/wp-stat/pwapi/*","action":"cancel"},{"selector":"*://cdn.tinypass.com/api/tinypass.min.js","action":"cancel"},{"selector":"*://api.tinypass.com/tpl/*","action":"cancel"},{"selector":"*://tm.jsuol.com.br/modules/content-gate.js","action":"cancel"},{"selector":"*://gauchazh.clicrbs.com.br/static/main*","action":"cancel"},{"selector":"*://www.rbsonline.com.br/cdn/scripts/special-paywall.min.js*","action":"cancel"},{"selector":"https://paywall.nsctotal.com.br/behaviors","action":"cancel"},{"selector":"*://*.estadao.com.br/paywall/*","action":"cancel"},{"selector":"*://www.folhadelondrina.com.br/login.php*","action":"cancel"},{"selector":"https://www.eltiempo.com/js/desktopArticle.js*","action":"cancel"},{"selector":"*://*.haaretz.co.il/*/inter.js","action":"cancel"},{"selector":"*://*.themarker.com/*/inter.js","action":"cancel"},{"selector":"*://*.diarinho.com.br/wp-admin/admin-ajax.php","action":"cancel"},{"selector":"*://diarinho.com.br/wp-admin/admin-ajax.php","action":"cancel"},{"selector":"*://static.infoglobo.com.br/paywall/js/tiny.js","action":"cancel"}]
// @run-at       document-start
// @noframes
// ==/UserScript==

// run_at: document_start
if (/gauchazh\\.clicrbs\\.com\\.br/.test(document.location.host)) {
  document.addEventListener('DOMContentLoaded', function() {
    function patchJs(jsurl) {
      GM_xmlhttpRequest({
        method: 'GET',
        url: jsurl,
        onload: function(response) {
          var injectme = response.responseText;
          injectme = injectme.replace(/[a-z].showLoginPaywall,/g, 'false,');
          injectme = injectme.replace(/[a-z].showPaywall,/g, 'false,');
          injectme = injectme.replace(/[a-z].requestCPF\\|\\|!1,/g, 'false,');
          injectme = injectme.replace(
            /![a-z].showLoginPaywall&&![a-z].showPaywall\\|\\|!1/g, 'true');
          var script = document.createElement('script');
          script.type = 'text/javascript';
          var textNode = document.createTextNode(injectme);
          script.appendChild(textNode);
          document.head.appendChild(script);
        }
      });
    }

    var scripts = Array.from(document.getElementsByTagName('script'));
    var script = scripts.find((el) => { return el.src.includes('static/main'); });
    if (script)
      patchJs(script.src);
  });

  window.onload = function() {
    function check(){
      if(document.getElementsByClassName('wrapper-paid-content')[0]){
        document.getElementsByClassName('wrapper-paid-content')[0].innerHTML = '<p>Por favor aperte Ctrl-F5 para carregar o restante da notícia!</p>';
      }
      setTimeout(function(){ check(); }, 1000);
    }
    check();
  };
}


else if (/jota\\.info/.test(document.location.host)) {
  var page_url = window.location.href;
  if (page_url.search('paywall') >= 0) { // Só ativa em urls com paywall
    var new_page_url = window.location.href.replace('www.jota.info/paywall?redirect_to=//', '');
    GM_xmlhttpRequest({
      method: 'GET',
      url: new_page_url,
      headers: {
        'User-Agent': 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
      },
      anonymous: true,
      onload: function(response) {
        var parser = new DOMParser();
        var newDocument = parser.parseFromString(response.responseText,'text/html');
        newDocument.getElementsByClassName('jota-paywall')[0].remove(); // Já remove o anúncio do paywall antes de inserir
        if (newDocument) {
          document.open();
          history.pushState({urlPath: new_page_url}, '', new_page_url); // Atualiza a url sem fazer um novo refresh
          document.write(newDocument.getElementsByTagName('html')[0].innerHTML);
          document.close();
        }
      }
    });
  }
}

else if (/crusoe\\.com\\.br/.test(document.location.host)) {
  document.cookie = 'crs_subscriber=1';
}

// run_at: document_idle
document.addEventListener('DOMContentLoaded', function() {
  var code = null;

  if (/www\\.economist\\.com/.test(document.location.host))
    code = 'document.cookie = "ec_limit=allow";';

  else if (/ft\\.com/.test(document.location.host)
      && document.querySelector('.barrier')) {

    eraseAllCookies();

    document.cookie = '';
    localStorage.clear();
    sessionStorage.clear();
    indexedDB.deleteDatabase('next-flags');
    indexedDB.deleteDatabase('next:ads');

    GM_xmlhttpRequest({
      method: 'GET',
      url: window.location.href,
      headers: {
        'Referer': 'https://www.google.com.br/'
      },
      anonymous: true,
      onload: function(response) {
        var parser = new DOMParser();
        var newDocument = parser.parseFromString(response.responseText,'text/html');
        if (newDocument.getElementsByClassName('article__content')[0]) {
          document.open();
          document.write(newDocument.getElementsByTagName('html')[0].innerHTML);
          document.close();
        }
      }
    });
  }

  else if (/foreignpolicy\\.com/.test(document.location.host)) {
    code = \`
      document.getElementById("paywall_bg").remove();
      document.body.classList.remove("overlay-no-scroll");
      document.body.style.overflow = "visible";
      document.documentElement.classList.remove("overlay-no-scroll");
    \`;
  }

  else if (/folha\\.uol\\.com\\.br/.test(document.location.host)) {
    code = \`
      omtrClickUOL = function(){};function showText() {
         $("#bt-read-more-content").next().show();
         $("#bt-read-more-content").next().show().prev().remove();
      }
      setTimeout(showText, 100);
    \`;
  }

  else if (/abril\\.com\\.br/.test(document.location.host))
    code = \`
      document.queryselectorall('.callpaywall')
        .foreach(x => x.remove());
      document.queryselectorall('.content-blocked')
        .foreach(x => x.classlist.remove('content-blocked'))
    \`;

  else if(/correio24horas\\.com\\.br/.test(document.location.host))
    // remover tudo relacionado ao paywall e remover limite de altura no div do conteúdo da matéria
    // verificar se a altura não buga com a mudança de largura da página (layout responsivo, né)
    code=\`
      jQuery('[class^=paywall]').remove();
      jQuery('[class^=blocked]').removeClass();
      jQuery('[id^=paywall]').removeClass('hide').removeClass('is-active');
      jQuery('.noticias-single__content__text').attr('style', 'height:auto;');
      jQuery('[id^=paywall]').remove();
      setInterval(function() { jQuery('[itemprop^=articleBody]').css('height', '100%'); console.log('Burlesco: forçando altura...'); }, 1000);

    \`;

  else if (/nytimes\\.com/.test(document.location.host))
    eraseAllCookies();

  else if (/wsj\\.com/.test(document.location.host)
      && document.querySelector('.wsj-snippet-login')) {

    eraseAllCookies();

    document.cookie = '';
    localStorage.clear();
    sessionStorage.clear();

    GM_xmlhttpRequest({
      method: 'GET',
      url: window.location.href,
      headers: {
        'Referer': 'https://www.facebook.com/'
      },
      anonymous: true,
      onload: function(response) {
        var parser = new DOMParser();
        var newDocument = parser.parseFromString(response.responseText,'text/html');
        if (newDocument.querySelector('article')) {
          document.body = newDocument.body;
        }
      }
    });
  }

  else if (/bloomberg\\.com/.test(document.location.host)) {
    localStorage.clear();
    sessionStorage.clear();
  }

  else if (/diariodaregiao\\.com\\.br/.test(document.location.host)) 
  {
    document.getElementsByClassName('noticia-texto')[0].style.display = 'block';
    document.querySelector('.conteudo > .row').style.display = 'none';
  }  

  else if (/diariopopular\\.com\\.br/.test(document.location.host)) {
    eraseAllCookies();
  }

  else if (/wired\\.com/.test(document.location.host)) {
    code = \`
      window.onload = function() {
        style = document.createElement('style');
        style.type = 'text/css';
        css='.paywall-container-component {display: none !important}';
        style.appendChild(document.createTextNode(css));
        document.head.appendChild(style);
      }
      document.cookie = "";
      localStorage.clear();
      sessionStorage.clear();
    \`;
    eraseAllCookies();
  }

  else if (/haaretz\\.com/.test(document.location.host) ||
          (/haaretz\\.co\\.il/.test(document.location.host))) {

    GM_xmlhttpRequest({
      method: 'GET',
      url: window.location.href,
      headers: {
        'User-Agent': 'Googlebot/2.1 (+http://www.googlebot.com/bot.html)'
      },
      anonymous: true,
      onload: function(response) {
        var parser = new DOMParser();
        var newDocument = parser.parseFromString(response.responseText,'text/html');
        if (newDocument) {
          document.open();
          document.write(newDocument.getElementsByTagName('html')[0].innerHTML);
          document.close();
        }
      }
    });
  }
  
  else if (/dgabc\\.com\\.br/.test(document.location.host)) {
    code = \`
      var email = 'colaborador@dgabc.com.br';
      var senha = '';
      localStorage.emailNoticiaExclusiva = email;
      $('.NoticiaExclusivaNaoLogado').hide();
      $('.NoticiaExclusivaLogadoSemPermissao').hide();
      $('.linhaSuperBanner').show();
      $('.footer').show();
      $('.NoticiaExclusivaLogado').show();
    \`;
  }
  
  else if (/em\\.com\\.br/.test(document.location.host)) {
    window.id_acesso_noticia = 0;
      
    let style = document.createElement('style');
    style.type = 'text/css';

    let css=\`
      .news-blocked {
        display: none !important
      }
      .news-blocked-no-scroll {
        overflow: auto !important;
        width: auto !important;
        position: unset !important;
      }
      
      div[itemprop="articleBody"] {
        height: auto !important;
      }
    \`;

    style.appendChild(document.createTextNode(css));
    document.head.appendChild(style);
  }

  else if (/newsweek\\.pl|forbes\\.pl/.test(document.location.host)) {
    let contentPremium = document.querySelector('.contentPremium');
    if (contentPremium) {
      contentPremium.classList.remove('contentPremium');
    }
  }

  if (code !== null) {
    var script = document.createElement('script');
    script.textContent = code;
    (document.head||document.documentElement).appendChild(script);
    script.parentNode.removeChild(script);
  }
});

function eraseAllCookies() {
  var cookieList  = document.cookie.split (/;\\s*/);
  for (var J = cookieList.length - 1;   J >= 0;  --J) {
    var cookieName = cookieList[J].replace (/\\s*(\\w+)=.+$/, '$1');
    eraseCookie (cookieName);
  }
}

function eraseCookie (cookieName) {
  // https://stackoverflow.com/a/28081337/1840019
  //--- ONE-TIME INITS:
  //--- Set possible domains. Omits some rare edge cases.?.
  var domain      = document.domain;
  var domain2     = document.domain.replace (/^www\\./, '');
  var domain3     = document.domain.replace (/^(\\w+\\.)+?(\\w+\\.\\w+)$/, '$2');

  //--- Get possible paths for the current page:
  var pathNodes   = location.pathname.split ('/').map ( function (pathWord) {
    return '/' + pathWord;
  } );
  var cookPaths   = [''].concat (pathNodes.map ( function (pathNode) {
    if (this.pathStr) {
      this.pathStr += pathNode;
    }
    else {
      this.pathStr = '; path=';
      return (this.pathStr + pathNode);
    }
    return (this.pathStr);
  } ) );

  // eslint-disable-next-line no-func-assign
  ( eraseCookie = function (cookieName) {
    //--- For each path, attempt to delete the cookie.
    cookPaths.forEach ( function (pathStr) {
      //--- To delete a cookie, set its expiration date to a past value.
      var diagStr     = cookieName + '=' + pathStr + '; expires=Thu, 01-Jan-1970 00:00:01 GMT;';
      document.cookie = diagStr;

      document.cookie = cookieName + '=' + pathStr + '; domain=' + domain  + '; expires=Thu, 01-Jan-1970 00:00:01 GMT;';
      document.cookie = cookieName + '=' + pathStr + '; domain=' + domain2 + '; expires=Thu, 01-Jan-1970 00:00:01 GMT;';
      document.cookie = cookieName + '=' + pathStr + '; domain=' + domain3 + '; expires=Thu, 01-Jan-1970 00:00:01 GMT;';
    } );
  } ) (cookieName);
}


}(console)</script></body>`)

  console.log('添加 tamperJS：burlesco.user.js')
}

$done({ body })