/**
 * 名称：MIT Technology Review Paywall Remover.user.js
 * 地址：https://greasyfork.org/scripts/29271-mit-technology-review-paywall-remover/code/MIT%20Technology%20Review%20Paywall%20Remover.user.js
 *
 ******** 以下为 tamperJS 自动生成的 rewrite 相关信息，可能需要根据情况适当调整 ********

[rewrite]
http.*:\/\/technologyreview\.com\/ url script-response-body MIT Technology Review Paywall Remover.user.js
http.*:\/\/www\.technologyreview\.com\/ url script-response-body MIT Technology Review Paywall Remover.user.js

[mitm]
, technologyreview.com, www.technologyreview.com

 ********
 * 工具: tamperJS BY @elecV2
 * 频道: https://t.me/elecV2
 *
**/

let body = $response.body

if (/<\/html>|<\/body>/.test(body)) {
  body = body.replace('</body>', `

<script>const elecJSPack = function(elecV2){

// ==UserScript==
// @name MIT Technology Review Paywall Remover
// @author noname120
// @namespace HandyUserscripts
// @description Remove the paywall on MIT Technology Review
// @version 1
// @license Creative Commons BY-NC-SA

// @include http*://technologyreview.com/
// @include http*://www.technologyreview.com/

// @grant none
// @run-at document-start
// ==/UserScript==

localStorage.removeItem('mittr:meter');
}(console)</script></body>`)

  console.log('添加 tamperJS：MIT Technology Review Paywall Remover.user.js')
}

$done({ body })